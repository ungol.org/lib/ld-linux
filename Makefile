.POSIX:

CC=cc
CFLAGS=@CFLAGS
OBJS=ld-linux.o ld-open.o _start.o __printf.o

all: hello ld-linux.so

ld-linux.so: $(OBJS)
	$(CC) -o $@ $(OBJS) @LDFLAGS

ld-linux.o: ld-linux.c
	$(CC) $(CFLAGS) -c ld-linux.c

__printf.o: __printf.c
	$(CC) $(CFLAGS) -c __printf.c

ld-open.o: ld-open.c
	$(CC) $(CFLAGS) -c ld-open.c

_start.o: _start.s
	$(CC) $(CFLAGS) -c _start.s

hello: hello.c
	$(CC) -o $@ hello.c -Wl,--dynamic-linker=$$(pwd)/ld-linux.so

clean:
	rm -f hello *.o *.so
