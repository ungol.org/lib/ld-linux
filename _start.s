.global main
.type main, %function

.global _dlstart
.type _dlstart, %function
_dlstart:
	popq %rdi
	movq %rsp, %rsi
	call main

	mov %rax, %rdi
	mov $60, %rax
	syscall

	hlt


.global _syscall
.type _syscall, %function
_syscall:
	mov %rdi, %rax
	mov %rsi, %rdi
	mov %rdx, %rsi
	mov %rcx, %rdx
	mov %r8,  %r10
	mov %r9,  %r8
	mov 8(%rsp), %r9
	syscall
	ret
