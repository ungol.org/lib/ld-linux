#ifndef HANDLE_H
#define HANDLE_H

#include <sys/types.h>
#include <sys/stat.h>
#include <elf.h>

struct dl_handle {
	Elf64_Ehdr *ehdr;

	void *phpage;
	Elf64_Phdr *phdr;

	void *shpage;
	Elf64_Shdr *shdr;
};

#endif
