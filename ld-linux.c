#include <stddef.h>
#include <elf.h>

#include "handle.h"

void *ld_open(const char *, int);
int printf(const char *, ...);

#define isprint(c) (32 <= c && c < 127)

int main(int argc, char *argv[])
{
	for (int i = 0; i < argc; i++) {
		printf("argv[%d]: '%s'\n", i, argv[i]);
	}

	struct dl_handle *handle = ld_open(argv[0], 0);
	if (handle == NULL) {
		printf("couldn't load %s\n", argv[0]);
		return 1;
	}

	printf("loaded at %p\n", (void*)handle);

	Elf64_Ehdr *ehdr = handle->ehdr;

	Elf64_Phdr *phdr = handle->phdr;
	int nphdr = ehdr->e_phnum;
	int phsize = ehdr->e_phentsize;

	Elf64_Shdr *shdr = handle->shdr;
	int nshdr = ehdr->e_shnum;
	int shsize = ehdr->e_shentsize;

	printf("ELF header:     %p\n", (void*)ehdr);
	printf("Program header: %p\n", (void*)phdr);
	printf("Section header: %p\n", (void*)shdr);

	for (int i = 0; i < nphdr; i++) {
		printf("\nProgram header %d: ", i);
		switch (phdr->p_type) {
		case PT_NULL:		printf("NULL\n"); break;
		case PT_LOAD:		printf("LOAD\n"); break;
		case PT_DYNAMIC:	printf("DYNAMIC\n"); break;
		case PT_INTERP:		printf("INTERP\n"); break;
		case PT_NOTE:		printf("NOTE\n"); break;
		case PT_SHLIB:		printf("SHLIB\n"); break;
		case PT_PHDR:		printf("PHDR\n"); break;
		case PT_LOOS:		printf("LOOS\n"); break;
		case PT_HIOS:		printf("HIOS\n"); break;
		case PT_LOPROC:		printf("LOPROC\n"); break;
		case PT_HIPROC:		printf("HIPROC\n"); break;
		default:		printf("Unknown %x\n", phdr->p_type);
		}

		printf("flags:  %x\n", phdr->p_flags);
		printf("offset: %lx\n", phdr->p_offset);
		printf("vaddr:  %lx\n", phdr->p_vaddr);
		printf("paddr:  %lx\n", phdr->p_paddr);
		printf("filesz: %lx\n", phdr->p_filesz);
		printf("memsz:  %lx\n", phdr->p_memsz);
		printf("align:  %lx\n", phdr->p_align);

		printf("data:   ");
		char *data = (char*)phdr->p_vaddr;
		for (size_t i = 0; i < phdr->p_filesz; i++) {
			if (isprint(data[i])) {
				printf("%c", data[i]);
			} else {
				//printf("\\x%x", data[i]);
				printf(".");
			}
		}
		printf("\n\n");

		phdr = (void*)((char*)(handle->phdr) + (i * phsize));
	}

	for (int i = 0; i < nshdr; i++) {
		printf("Section header %d: ", i);
		switch (shdr->sh_type) {
		case SHT_NULL:		printf("NULL\n"); break;
		case SHT_PROGBITS:	printf("PROGBITS\n"); break;
		case SHT_SYMTAB:	printf("SYMTAB\n"); break;
		case SHT_STRTAB:	printf("STRTAB\n"); break;
		case SHT_RELA:		printf("RELA\n"); break;
		case SHT_HASH:		printf("HASH\n"); break;
		case SHT_DYNAMIC:	printf("DYNAMIC\n"); break;
		case SHT_NOTE:		printf("NOTE\n"); break;
		case SHT_NOBITS:	printf("NOBITS\n"); break;
		case SHT_REL:		printf("REL\n"); break;
		case SHT_SHLIB:		printf("SHLIB\n"); break;
		case SHT_DYNSYM:	printf("DYNSYM\n"); break;
		case SHT_INIT_ARRAY:	printf("INIT_ARRAY\n"); break;
		case SHT_FINI_ARRAY:	printf("FINI_ARRAY\n"); break;
		case SHT_PREINIT_ARRAY:	printf("PREINIT_ARRAY\n"); break;
		case SHT_GROUP:		printf("GROUP\n"); break;
		case SHT_SYMTAB_SHNDX:	printf("SYMTAB_SHNDX\n"); break;
		case SHT_NUM:		printf("NUM\n"); break;
		case SHT_LOOS:		printf("LOOS\n"); break;
		default:		printf("Unknown: %x\n", shdr->sh_type);
		}

		printf("flags:     %lx\n", shdr->sh_flags);
		printf("offset:    %lx\n", shdr->sh_offset);
		printf("addr:      %lx\n", shdr->sh_addr);
		printf("size:      %lx\n", shdr->sh_size);
		printf("link:      %x\n", shdr->sh_link);
		printf("info:      %x\n", shdr->sh_info);
		printf("addralign: %lx\n", shdr->sh_addralign);
		printf("entsize:   %lx\n", shdr->sh_entsize);

		printf("data:      ");
		char *data = (char*)shdr->sh_addr;
		for (size_t i = 0; i < shdr->sh_size; i++) {
			if (isprint(data[i])) {
				printf("%c", data[i]);
			} else {
				printf(".");
			}
		}
		printf("\n\n");

		shdr = (void*)((char*)(handle->shdr) + (i * shsize));
	}

	return 0;
}
