#define _GNU_SOURCE
#include <elf.h>
#include <stddef.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include "mmap.h"
#include "handle.h"

extern long _syscall(long, ...);
extern int printf(const char *fmt, ...);

#define close(_fd) \
	_syscall(3, _fd)

#define open(_path, _amode) \
	_syscall(2, _path, _amode)

#define fstat(_fd, _buf) \
	_syscall(5, _fd, _buf)

#ifdef __amd64__
#define WORDSIZE 64
#define ELF_CLASS 2
#define ELF_MACHINE 0x3e
#else
#define WORDSIZE 32
#define ELF_CLASS 1
#define ELF_MACHINE 0x03
#endif

#define PAGESIZE 4096

#define _map(_fd, _off, _len) \
	mmap(NULL, _len, PROT_READ | PROT_WRITE, MAP_PRIVATE, _fd, _off)

static void *fmap(int fd, size_t offset, size_t flen, size_t mlen)
{
	size_t rem = offset % PAGESIZE;
	size_t len = flen > mlen ? flen : mlen;

	if (rem == 0) {
		return _map(fd, offset, len);
	}

	char *base = _map(fd, offset - rem, flen + rem);
	char *page = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

	for (size_t i = 0; i < flen; i++) {
		page[i] = base[rem + i];
	}

	munmap(base, flen + rem);

	return page;
}

void *ld_open(const char *file, int mode)
{
	(void)mode;

	int fd = open(file, O_RDONLY);
	if (fd < 0) {
		return NULL;
	}

	Elf64_Ehdr *ehdr = _map(fd, 0, sizeof(*ehdr));
	if (ehdr == MAP_FAILED) {
		return NULL;
	}

	if (ehdr->e_ident[EI_MAG0] != '\x7f' ||
		ehdr->e_ident[EI_MAG1] != 'E' ||
		ehdr->e_ident[EI_MAG2] != 'L' ||
		ehdr->e_ident[EI_MAG3] != 'F')
	{
		/* invalid magic */
		printf("bad magic: %hhx%hhx%hhx%hhx\n", ehdr->e_ident[EI_MAG0],
                	ehdr->e_ident[EI_MAG1],
                	ehdr->e_ident[EI_MAG2],
                	ehdr->e_ident[EI_MAG3]);
		return NULL;
	}

	if (ehdr->e_ident[EI_CLASS] != ELF_CLASS) {
		/* 1 = 32-bit */
		/* 2 = 64-bit */
		printf("bad class: %d\n", ehdr->e_ident[EI_CLASS]);
		return NULL;
	}

	int endian = ehdr->e_ident[EI_DATA];
	if (endian != 1 && endian != 2) {
		/* 1 = little */
		/* 2 = big */
		printf("bad endian: %d\n", ehdr->e_ident[EI_DATA]);
		return NULL;
	}

	if (ehdr->e_ident[EI_VERSION] != 1) {
		/* 1 = current version of ELF */
		printf("bad version: %d\n", ehdr->e_ident[EI_VERSION]);
		return NULL;
	}

	if (ehdr->e_ident[EI_OSABI] != 0x0 && ehdr->e_ident[EI_OSABI] != 0x3) {
		/* 0x0 = no specific ABI */
		/* 0x3 = Linux */
		printf("bad abi: %d\n", ehdr->e_ident[EI_OSABI]);
		return NULL;
	}

	/* ignore EI_ABIVERSION, that's some GNU libc nonsense */

	/* check e_type in main(); */

	if (ehdr->e_machine != ELF_MACHINE) {
		printf("bad machine: %d\n", ehdr->e_machine);
		return NULL;
	}

	if (ehdr->e_version != 1) {
		/* version again, for some reason */
		printf("bad version: %d\n", ehdr->e_version);
		return NULL;
	}

	/* use e_entry in main() */

	/* e_flags - architecture dependent */
	/* e_ehsize - sizeof(*ehdrd) */
	/* e_phentsize - sizeof(*phdr) */
	/* e_phnum - number of entries in phdr */
	/* e_shentsize - sizeof(*shrd) */
	/* e_shnum - number of entries in shdr */
	/* e_shstrndx - shdr[e_shstrndx] == section names */

	size_t s = sizeof(struct dl_handle) + (sizeof(void*) * (ehdr->e_phnum + ehdr->e_shnum));
	struct dl_handle*h = mmap(NULL, s, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (h == MAP_FAILED) {
		printf("couldn't allocate memory for handle\n");
		return NULL;
	}

	h->ehdr = ehdr;

	/* map progam headers and related segments */
	size_t phpage = ehdr->e_phoff - (ehdr->e_phoff % PAGESIZE);
	size_t phlen = (ehdr->e_phentsize * ehdr->e_phnum) + (ehdr->e_phoff % PAGESIZE);
	h->phpage = _map(fd, phpage, phlen);
	if (h->phpage == MAP_FAILED) {
		printf("couldn't map phdr\n");
		return NULL;
	}
	h->phdr = (void*)((char*)h->phpage + (ehdr->e_phoff % PAGESIZE));

	Elf64_Phdr *ph = h->phdr;
	for (int i = 0; i < ehdr->e_phnum; i++) {
		ph[i].p_vaddr = (long)fmap(fd, ph[i].p_offset, ph[i].p_filesz, ph[i].p_memsz);
	}

	mprotect(h->phpage, phlen, PROT_READ);


	/* map section headers and related sections */
	size_t shpage = ehdr->e_shoff - (ehdr->e_shoff % PAGESIZE);
	size_t shlen = (ehdr->e_shentsize * ehdr->e_shnum) + (ehdr->e_shoff % PAGESIZE);
	h->shpage = _map(fd, shpage, shlen);
	if (h->shpage == MAP_FAILED) {
		printf("couldn't map shdr\n");
		return NULL;
	}
	h->shdr = (void*)((char*)h->shpage + (ehdr->e_shoff % PAGESIZE));

	Elf64_Shdr *sh = h->shdr;
	for (int i = 0; i < ehdr->e_shnum; i++) {
		sh[i].sh_addr = (long)fmap(fd, sh[i].sh_offset, sh[i].sh_size, sh[i].sh_size);
	}

	mprotect(h->shpage, shlen, PROT_READ);

	close(fd);

	return h;
}
