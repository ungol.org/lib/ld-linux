#define _XOPEN_SOURCE 700
#include <sys/mman.h>

extern long _syscall(long, ...);

#define mmap(_addr, _len, _prot, _flags, _fd, _off) \
	(void*)_syscall(9, _addr, _len, _prot, _flags, _fd, _off)

#define munmap(_addr, _len) \
	_syscall(11, _addr, _len)

#define mprotect(_addr, _len, _prot) \
	_syscall(10, _addr, _len, _prot)
